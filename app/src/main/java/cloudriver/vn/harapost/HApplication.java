package cloudriver.vn.harapost;

import android.app.Application;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import cloudriver.vn.harapost.network.ApiService;
import cloudriver.vn.harapost.util.AppDefine;
import cloudriver.vn.harapost.util.AppStorage;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class HApplication extends Application {
    private static HApplication sInstance;
    private ApiService apiService;
    private AppStorage appStorage;
    private AppDefine appDefine;


    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        appStorage = new AppStorage(this);
        appDefine = new AppDefine(this);

    }

    public synchronized static HApplication getInstance() {
        return sInstance;
    }

    public boolean isShipper() {
        return getAppStorage().getUser().getType() == 1;
    }

    public AppStorage getAppStorage() {
        return appStorage;
    }

    public AppDefine getAppDefine() {
        return appDefine;
    }

    public ApiService getApiService() {
        if (apiService == null) {
            apiService = provideRetrofit().create(ApiService.class);
        }
        return apiService;
    }

    public void refreshToken() {
        apiService = null;
    }

    private Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(ApiService.URL)
                .client(provideOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    private OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder okhttpClientBuilder = new OkHttpClient.Builder();

        okhttpClientBuilder.addNetworkInterceptor(chain -> {
            Request request = chain.request().newBuilder()
                    .addHeader("Authorization", String.format("Bearer %s", getAppStorage().getToken()))
                    .build();
            return chain.proceed(request);
        });

        okhttpClientBuilder.connectTimeout(30, TimeUnit.SECONDS);
        okhttpClientBuilder.readTimeout(30, TimeUnit.SECONDS);
        okhttpClientBuilder.writeTimeout(30, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.level(HttpLoggingInterceptor.Level.BODY);
            okhttpClientBuilder.addInterceptor(logging);
        }
        return okhttpClientBuilder.build();
    }
}

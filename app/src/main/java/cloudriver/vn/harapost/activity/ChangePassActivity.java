package cloudriver.vn.harapost.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import butterknife.BindView;
import cloudriver.vn.harapost.R;
import cloudriver.vn.harapost.presenter.ChangePassPresenter;
import cloudriver.vn.harapost.view.ChangePassView;

public class ChangePassActivity extends BaseActivity implements ChangePassView, View.OnClickListener {
    @InjectPresenter
    ChangePassPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.pass_old_inputlayout)
    TextInputLayout pass_old_inputlayout;
    @BindView(R.id.pass_new_inputlayout)
    TextInputLayout pass_new_inputlayout;
    @BindView(R.id.pass_renew_inputlayout)
    TextInputLayout pass_renew_inputlayout;
    @BindView(R.id.pass_old_edt)
    TextInputEditText pass_old_edt;
    @BindView(R.id.pass_new_edt)
    TextInputEditText pass_new_edt;
    @BindView(R.id.pass_renew_edt)
    TextInputEditText pass_renew_edt;
    @BindView(R.id.change_pass_button)
    View change_pass_button;
    @BindView(R.id.progressBar)
    View progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepass);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            setTitle(R.string.change_pass);
        }
    }


    void showLoading() {
        pass_old_edt.setEnabled(false);
        pass_new_edt.setEnabled(false);
        pass_renew_edt.setEnabled(false);
        change_pass_button.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }


    @Override
    public void onClick(View v) {
        String oldpass = Objects.requireNonNull(pass_old_edt.getText()).toString();
        String password = Objects.requireNonNull(pass_new_edt.getText()).toString();
        String repassword = Objects.requireNonNull(pass_renew_edt.getText()).toString();

        boolean isValid = true;
        if (TextUtils.isEmpty(password)) {
            pass_new_inputlayout.setError("Vui lòng nhập mật khẩu mới");
            pass_new_edt.requestFocus();
            isValid = false;
        }
        if (TextUtils.isEmpty(repassword)) {
            pass_renew_inputlayout.setError("Nhập lại mật khẩu không đúng");
            pass_renew_edt.requestFocus();
            isValid = false;
        }
        if (TextUtils.isEmpty(oldpass)) {
            pass_old_inputlayout.setError("Vui lòng nhập mật khẩu");
            pass_old_edt.requestFocus();
            isValid = false;
        }

        if (isValid) {
            showLoading();
            presenter.changePass(oldpass, password);
        }
    }

    @Override
    public void onSuccess(Boolean data) {
        Toast.makeText(this, "Đổi mật khẩu thành công", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onError(String error) {
        pass_old_edt.setEnabled(true);
        pass_new_edt.setEnabled(true);
        pass_renew_edt.setEnabled(true);
        change_pass_button.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}

package cloudriver.vn.harapost.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.mpt.android.stv.Slice;
import com.mpt.android.stv.SpannableTextView;

import butterknife.BindColor;
import butterknife.BindDimen;
import butterknife.BindView;
import cloudriver.vn.harapost.HApplication;
import cloudriver.vn.harapost.R;
import cloudriver.vn.harapost.entries.Order;
import cloudriver.vn.harapost.entries.OrderDetail;
import cloudriver.vn.harapost.entries.OrderImport;
import cloudriver.vn.harapost.util.Utils;

public class DetailOrderActivity extends BaseActivity {
    private static final String TAG = "DetailOrderActivity";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.info_text)
    SpannableTextView info_text;
    @BindView(R.id.sender_text)
    SpannableTextView sender_text;
    @BindView(R.id.receiver_text)
    SpannableTextView receiver_text;

    @BindColor(R.color.info_blue)
    int color_blue;
    @BindColor(R.color.info_purple)
    int color_purple;
    @BindColor(R.color.info_green)
    int color_green;
    @BindColor(R.color.grey_700)
    int color_grey;
    @BindColor(R.color.detail_status)
    int color_status;

    @BindColor(R.color.order_picking)
    int order_picking;
    @BindColor(R.color.order_shipping)
    int order_shipping;
    @BindColor(R.color.order_failed)
    int order_failed;
    @BindColor(R.color.order_success)
    int order_success;

    @BindDimen(R.dimen.text_header)
    int size_header;
    @BindDimen(R.dimen.space_2dp)
    int border_round;


    Order order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Bundle b = getIntent().getExtras();

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            setTitle("Chi tiết đơn hàng");
        }

        if (b != null) {
            if (b.containsKey("order")) {
                OrderDetail detail = b.getParcelable("order");
                Log.e(TAG, new Gson().toJson(detail));
                order = detail.getOrder();
            } else {
                OrderImport orderImport = b.getParcelable("import");
                Log.e(TAG, new Gson().toJson(orderImport));
                order = orderImport.getOrder();
            }
            showInfo();
            showSender();
            showReceiver();
        }
    }

    void showInfo() {
        info_text.reset();
        info_text.addSlice(new Slice.Builder("THÔNG TIN")
                .textColor(color_blue)
                .textSize(size_header)
                .build());

        info_text.addSlice(new Slice.Builder("\n\nMã đơn: ")
                .textColor(color_grey).build());
        info_text.addSlice(new Slice.Builder(order.getCode())
                .build());
        info_text.addSlice(new Slice.Builder("\n\nGiá trị đơn hàng: ")
                .textColor(color_grey).build());
        info_text.addSlice(new Slice.Builder(Utils.moneyFormat(order.getAmount()))
                .build());
        info_text.addSlice(new Slice.Builder("\n\nCước vận chuyển: ")
                .textColor(color_grey).build());
        info_text.addSlice(new Slice.Builder(Utils.moneyFormat(order.getFee_transport()))
                .build());
        info_text.addSlice(new Slice.Builder("\n\nPhí giao hàng: ")
                .textColor(color_grey).build());
        info_text.addSlice(new Slice.Builder(Utils.moneyFormat(order.getFee_ship()))
                .build());
        info_text.addSlice(new Slice.Builder("\n\nPhí thu hộ: ")
                .textColor(color_grey).build());
        info_text.addSlice(new Slice.Builder(Utils.moneyFormat(order.getFee_cod()))
                .build());
        info_text.addSlice(new Slice.Builder("\n\nPhí đảm bảo: ")
                .textColor(color_grey).build());
        info_text.addSlice(new Slice.Builder(Utils.moneyFormat(order.getFee_safe()))
                .build());
        info_text.addSlice(new Slice.Builder("\n\nCOD: ")
                .textColor(color_grey).build());
        info_text.addSlice(new Slice.Builder(Utils.moneyFormat(order.getCod()))
                .build());
        info_text.addSlice(new Slice.Builder("\n\nPhí khác: ")
                .textColor(color_grey).build());
        info_text.addSlice(new Slice.Builder(Utils.moneyFormat(order.getFee_other()))
                .build());
        info_text.addSlice(new Slice.Builder("\n\nTổng phí: ")
                .textColor(color_grey).build());
        info_text.addSlice(new Slice.Builder(Utils.moneyFormat(order.getTotal_fee()))
                .build());
        info_text.addSlice(new Slice.Builder("\n\nNgười thanh toán phí: ")
                .textColor(color_grey).build());
        info_text.addSlice(new Slice.Builder(HApplication.getInstance().getAppDefine().getPaymentUser(order.getPayment_user()))
                .build());
        info_text.addSlice(new Slice.Builder("\n\nPhương thức giao nhận: ")
                .textColor(color_grey).build());
        info_text.addSlice(new Slice.Builder(HApplication.getInstance().getAppDefine().getMethodShip(order.getMethod_ship()))
                .build());
        info_text.addSlice(new Slice.Builder("\n\nTrạng thái: ")
                .textColor(color_grey)
                .build());
        info_text.addSlice(new Slice.Builder(String.format("  %s  ",
                HApplication.getInstance().getAppDefine().getOrderStatus(order.getStatus_id())))
                .backgroundColor(color_status)
                .textColor(Color.WHITE)
                .textSizeRelative(1.3f)
                .setCornerRadius(5)
                .build());
        info_text.display();
    }

    void showSender() {
        sender_text.reset();
        sender_text.addSlice(new Slice.Builder("ĐỊA CHỈ GỬI")
                .textColor(color_purple)
                .textSize(size_header)
                .build());

        sender_text.addSlice(new Slice.Builder("\n\nHọ tên: ")
                .textColor(color_grey).build());
        sender_text.addSlice(new Slice.Builder(order.getSender_name())
                .build());
        sender_text.addSlice(new Slice.Builder("\n\nĐiện thoại: ")
                .textColor(color_grey).build());
        sender_text.addSlice(new Slice.Builder(order.getSender_phone())
                .build());
        sender_text.addSlice(new Slice.Builder("\n\nĐịa chỉ: ")
                .textColor(color_grey).build());
        sender_text.addSlice(new Slice.Builder(String.format("%s, %s, %s", order.getSender_address(), order.getSender_district(), order.getSender_province()))
                .build());
        sender_text.display();
    }

    void showReceiver() {
        receiver_text.reset();
        receiver_text.addSlice(new Slice.Builder("ĐỊA CHỈ NHẬN")
                .textColor(color_green)
                .textSize(size_header)
                .build());

        receiver_text.addSlice(new Slice.Builder("\n\nHọ tên: ")
                .textColor(color_grey).build());
        receiver_text.addSlice(new Slice.Builder(order.getReceiver_name())
                .build());
        receiver_text.addSlice(new Slice.Builder("\n\nĐiện thoại: ")
                .textColor(color_grey).build());
        receiver_text.addSlice(new Slice.Builder(order.getReceiver_phone())
                .build());
        receiver_text.addSlice(new Slice.Builder("\n\nĐịa chỉ: ")
                .textColor(color_grey).build());
        receiver_text.addSlice(new Slice.Builder(String.format("%s, %s, %s", order.getReceiver_address(), order.getReceiver_district(), order.getReceiver_province()))
                .build());
        receiver_text.display();
    }
}

package cloudriver.vn.harapost.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import cloudriver.vn.harapost.R;
import cloudriver.vn.harapost.presenter.LoginPresenter;
import cloudriver.vn.harapost.view.LoginView;

public class LoginActivity extends BaseActivity implements LoginView {
    @InjectPresenter
    LoginPresenter presenter;

    @BindView(R.id.login_user_inputlayout)
    TextInputLayout login_user_inputlayout;
    @BindView(R.id.login_pass_inputlayout)
    TextInputLayout login_pass_inputlayout;
    @BindView(R.id.login_user_edt)
    TextInputEditText login_user_edt;
    @BindView(R.id.login_pass_edt)
    TextInputEditText login_pass_edt;
    @BindView(R.id.login_button)
    View login_button;
    @BindView(R.id.progressBar)
    View progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @OnClick(R.id.login_button)
    void login() {
        String email = Objects.requireNonNull(login_user_edt.getText()).toString();
        String password = Objects.requireNonNull(login_pass_edt.getText()).toString();

        boolean isValid = true;
        if (TextUtils.isEmpty(password)) {
            login_pass_inputlayout.setError("vui lòng nhập mật khẩu");
            login_pass_edt.requestFocus();
            isValid = false;
        }
        if (TextUtils.isEmpty(email)) {
            login_user_inputlayout.setError("vui lòng nhập email");
            login_user_edt.requestFocus();
            isValid = false;
        }

        if (isValid) {
            showLoading();
            presenter.login(email, password);
        }
    }

    void showLoading() {
        login_user_edt.setEnabled(false);
        login_pass_edt.setEnabled(false);
        login_button.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccess(String data) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onError(String error) {
        login_user_edt.setEnabled(true);
        login_pass_edt.setEnabled(true);
        login_button.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }
}

package cloudriver.vn.harapost.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.github.florent37.inlineactivityresult.InlineActivityResult;
import com.github.florent37.inlineactivityresult.Result;
import com.github.florent37.inlineactivityresult.callbacks.ActivityResultListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Objects;

import butterknife.BindView;
import cloudriver.vn.harapost.HApplication;
import cloudriver.vn.harapost.R;
import cloudriver.vn.harapost.fragment.HomeFragment;
import cloudriver.vn.harapost.fragment.StorekeeperFragment;
import cloudriver.vn.harapost.fragment.UserFragment;

import static com.github.florent37.runtimepermission.RuntimePermission.askPermission;

public class MainActivity extends BaseActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener {
    public static final String[] navigationTag = new String[]{"home", "setting"};

    @BindView(R.id.bottomBar)
    BottomNavigationView bottomBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomBar.setOnNavigationItemSelectedListener(this);
        setCurrentTab(0);
    }

    private void setCurrentTab(int position) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        String fragmentTag = navigationTag[position];

        //Show or add fragment by tab
        Fragment content = fragmentManager.findFragmentByTag(fragmentTag);
        if (content != null) {
            if (content.isVisible())
                return;
            else {
                transaction.show(content);
            }
        } else {
            Fragment newFragment;
            if (position == 0) {
                newFragment = HApplication.getInstance().isShipper()
                        ? HomeFragment.newInstance()
                        : StorekeeperFragment.newInstance();
            } else {
                newFragment = UserFragment.newInstance();
            }
            transaction.add(R.id.container, newFragment, fragmentTag);
        }
        //hide showing fragment
        String tag = navigationTag[position == 0 ? 1 : 0];
        Fragment other = fragmentManager.findFragmentByTag(tag);
        if (other != null)
            transaction.hide(other);

        transaction.commit();
    }

    void openQrScanner() {
        askPermission(this)
                .request(Manifest.permission.CAMERA)
                .onAccepted((resultPermission) -> {
                            InlineActivityResult.startForResult(this, new Intent(this, QrScanActivity.class),
                                    new ActivityResultListener() {
                                        @Override
                                        public void onSuccess(Result result) {
                                            if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                                                String qrcode = result.getData().getStringExtra("qrcode");
                                                QrDetailActivity.openQrDetai(MainActivity.this, qrcode);
                                            }
                                        }

                                        @Override
                                        public void onFailed(Result result) {

                                        }
                                    });
                        }
                )
                .onDenied((result) -> {
                })
                .onForeverDenied((result) ->
                        new AlertDialog.Builder(MainActivity.this)
                                .setMessage("Vui lòng cho phép truy cập Máy ảnh trong Cài đặt > Quyền")
                                .setPositiveButton(android.R.string.ok, (dialog, which) -> result.goToSettings()) // ask again
                                .setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss())
                                .show()
                )
                .ask();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.bottom_order:
                setCurrentTab(0);
                return true;
            case R.id.bottom_user:
                setCurrentTab(1);
                return true;
            default:
                openQrScanner();
                return false;
        }
    }
}

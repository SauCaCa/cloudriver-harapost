package cloudriver.vn.harapost.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.github.florent37.inlineactivityresult.InlineActivityResult;
import com.github.florent37.inlineactivityresult.Result;
import com.github.florent37.inlineactivityresult.callbacks.ActivityResultListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cloudriver.vn.harapost.R;
import cloudriver.vn.harapost.adapter.ItemImportDetailBinder;
import cloudriver.vn.harapost.adapter.ItemOrderDetailBinder;
import cloudriver.vn.harapost.adapter.OnViewholderClickListener;
import cloudriver.vn.harapost.entries.OrderDetail;
import cloudriver.vn.harapost.entries.OrderImport;
import cloudriver.vn.harapost.entries.PickedData;
import cloudriver.vn.harapost.presenter.QrDetailPresenter;
import cloudriver.vn.harapost.view.QrDetailView;
import cloudriver.vn.harapost.widget.ProgressDialog;
import me.drakeet.multitype.MultiTypeAdapter;

public class QrDetailActivity extends BaseActivity
        implements ActivityCompat.OnRequestPermissionsResultCallback,
        QrDetailView,
        View.OnClickListener {
    private static final String QRCODE = "qrcode";

    @InjectPresenter
    QrDetailPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private List<Object> list;
    ProgressDialog progressDialog;
    MultiTypeAdapter adapter;
    OnViewholderClickListener clicklistener, removeClicklistener;

    public static void openQrDetai(Context context, String qrcode) {
        Intent intent = new Intent(context, QrDetailActivity.class);
        intent.putExtra(QRCODE, qrcode);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrscan_detail);

        initView();
        getDataIntent();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getDataIntent();
    }

    void getDataIntent() {
        Bundle b = getIntent().getExtras();
        if (b != null && b.containsKey(QRCODE)) {
            String qrcode = b.getString(QRCODE);
            presenter.getQrDetail(qrcode);
        }
    }

    void initView() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            setTitle("Chi tiết đơn hàng");
        }
        clicklistener = (order, position) -> {

        };
        removeClicklistener = (order, position) -> {
            list.remove(position);
            adapter.notifyItemRemoved(position);
        };

        list = new ArrayList<>();
        adapter = new MultiTypeAdapter();
        adapter.register(OrderDetail.class, new ItemOrderDetailBinder(clicklistener, removeClicklistener));
        adapter.register(OrderImport.class, new ItemImportDetailBinder(clicklistener, removeClicklistener));
        adapter.setItems(list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    void showCamera() {
        InlineActivityResult.startForResult(this, new Intent(this, QrScanActivity.class),
                new ActivityResultListener() {
                    @Override
                    public void onSuccess(Result result) {
                        if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                            String qrcode = result.getData().getStringExtra("qrcode");
                            presenter.getQrDetail(qrcode);
                        }
                    }

                    @Override
                    public void onFailed(Result result) {

                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_scan:
                showCamera();
                break;
            case R.id.button_done:
                if (list.size() > 0) {
                    String[] ids = new String[list.size()];
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i) instanceof OrderDetail)
                            ids[i] = String.valueOf(((OrderDetail) list.get(i)).getShipment().getId());
                        else if (list.get(i) instanceof OrderImport)
                            ids[i] = String.valueOf(((OrderImport) list.get(i)).getImport().getId());
                    }
                    PickedData data = new PickedData(ids);
                    presenter.confirmList(data);
                }
                break;
        }
    }


    @Override
    public void showLoading() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
        }
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void hideLoading() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void confirmSuccess() {
        Toast.makeText(this, "Thành công", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void confirmFailed(String error) {
        hideLoading();
        new AlertDialog.Builder(this)
                .setTitle("Lỗi")
                .setMessage(error)
                .setCancelable(false)
                .setPositiveButton(getString(android.R.string.ok), (dialog, which) -> dialog.dismiss()).show();
    }

    @Override
    public void onSuccess(OrderDetail data) {
        hideLoading();
        boolean already = false;
        if (list.size() > 0) {
            for (Object o : list) {
                if (TextUtils.equals(((OrderDetail) o).getOrder().getCode(), data.getOrder().getCode())) {
                    already = true;
                    break;
                }
            }
        }
        if (!already) {
            list.add(data);
            adapter.notifyItemInserted(list.size() - 1);
        }
    }

    @Override
    public void onSuccess(OrderImport data) {
        hideLoading();
        boolean already = false;
        if (list.size() > 0) {
            for (Object o : list) {
                if (TextUtils.equals(((OrderImport) o).getOrder().getCode(), data.getOrder().getCode())) {
                    already = true;
                    break;
                }
            }
        }
        if (!already) {
            list.add(data);
            adapter.notifyItemInserted(list.size() - 1);
        }
    }

    @Override
    public void onError(String error) {
        hideLoading();
        new AlertDialog.Builder(this)
                .setTitle("Lỗi")
                .setMessage(error)
                .setCancelable(false)
                .setPositiveButton(getString(android.R.string.ok), (dialog, which) -> dialog.dismiss()).show();
    }
}

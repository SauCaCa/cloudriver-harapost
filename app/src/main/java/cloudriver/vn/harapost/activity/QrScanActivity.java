package cloudriver.vn.harapost.activity;

import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.View;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import butterknife.BindView;
import cloudriver.vn.harapost.R;

public class QrScanActivity extends BaseActivity
        implements QRCodeReaderView.OnQRCodeReadListener, View.OnClickListener {

    @BindView(R.id.qrdecoderview)
    QRCodeReaderView qrCodeReaderView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrscan);

        qrCodeReaderView.setAutofocusInterval(2000L);
        qrCodeReaderView.setQRDecodingEnabled(true);
        qrCodeReaderView.setOnQRCodeReadListener(this);
        qrCodeReaderView.setTorchEnabled(true);
        qrCodeReaderView.setBackCamera();
        qrCodeReaderView.startCamera();
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        Intent intent = new Intent();
        intent.putExtra("qrcode", text);
        setResult(RESULT_OK, intent);
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (qrCodeReaderView != null) {
            qrCodeReaderView.startCamera();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (qrCodeReaderView != null) {
            qrCodeReaderView.stopCamera();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        onBackPressed();
    }

}

package cloudriver.vn.harapost.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.arellomobile.mvp.presenter.InjectPresenter;

import cloudriver.vn.harapost.HApplication;
import cloudriver.vn.harapost.presenter.SplashPresenter;
import cloudriver.vn.harapost.view.SplashView;

public class SplashActivity extends BaseActivity implements SplashView {
    @InjectPresenter
    SplashPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.getDefine();
    }

    @Override
    public void onSuccess(Boolean data) {
        if (HApplication.getInstance().getAppStorage().isLoggedin()) {
            startActivity(new Intent(this, MainActivity.class));
        } else
            startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void onError(String error) {

    }
}

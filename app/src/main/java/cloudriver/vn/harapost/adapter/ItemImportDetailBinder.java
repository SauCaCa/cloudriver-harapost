package cloudriver.vn.harapost.adapter;

import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.mpt.android.stv.Slice;
import com.mpt.android.stv.SpannableTextView;

import java.util.Objects;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cloudriver.vn.harapost.R;
import cloudriver.vn.harapost.entries.Order;
import cloudriver.vn.harapost.entries.OrderDetail;
import cloudriver.vn.harapost.entries.OrderImport;
import me.drakeet.multitype.ItemViewBinder;

public class ItemImportDetailBinder extends ItemViewBinder<OrderImport, ItemImportDetailBinder.ViewHolder> {
    private OnViewholderClickListener clicklistener, removeListener;

    public ItemImportDetailBinder(OnViewholderClickListener clicklistener, OnViewholderClickListener removeListener) {
        this.clicklistener = clicklistener;
        this.removeListener = removeListener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_order_detail, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull OrderImport item) {
        holder.bind(item);
    }

    @Override
    protected long getItemId(@NonNull OrderImport item) {
        return item.hashCode();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.order_code)
        SpannableTextView textCode;
        @BindView(R.id.order_sender)
        SpannableTextView textSender;
        @BindView(R.id.order_receiver)
        SpannableTextView textReceiver;

        @BindColor(R.color.grey_700)
        int color_grey;

        OrderImport orderDetail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                if (clicklistener != null)
                    clicklistener.onClick(orderDetail, getAdapterPosition());
            });
        }

        void bind(OrderImport detail) {
            Log.e("bind", new Gson().toJson(detail));
            orderDetail = detail;
            Order order = detail.getOrder();

            textCode.reset();
            textCode.addSlice(new Slice.Builder("Mã: ")
                    .textColor(color_grey)
                    .build());
            textCode.addSlice(new Slice.Builder(order.getCode())
                    .style(Typeface.BOLD)
                    .textColor(Color.BLACK)
                    .build());
            textCode.display();

            textSender.reset();
            textSender.addSlice(new Slice.Builder("Nơi gửi: ")
                    .textColor(color_grey)
                    .build());
            textSender.addSlice(new Slice.Builder(String.format("%s, %s, %s",
                    order.getSender_address(), order.getSender_district(), order.getSender_province()))
                    .build());
            textSender.display();

            textReceiver.reset();
            textReceiver.addSlice(new Slice.Builder("Nơi nhận: ")
                    .textColor(color_grey)
                    .build());
            textReceiver.addSlice(new Slice.Builder(String.format("%s, %s, %s",
                    order.getReceiver_address(), order.getReceiver_district(), order.getReceiver_province()))
                    .build());
            textReceiver.display();
        }

        @OnClick({R.id.remove_button})
        void click(View v) {
            Objects.requireNonNull(removeListener).onClick(orderDetail, getAdapterPosition());
        }
    }
}

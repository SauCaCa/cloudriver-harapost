package cloudriver.vn.harapost.adapter;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Objects;

public class ListOrderDecoration extends RecyclerView.ItemDecoration {
    int padding;

    public ListOrderDecoration(int padding) {
        this.padding = padding;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        int center = padding / 2;

        if (position != RecyclerView.NO_POSITION) {
            if (position == 0) {
                outRect.left = padding;
                outRect.right = padding;
                outRect.top = padding;
                outRect.bottom = center;
            } else if (position == Objects.requireNonNull(parent.getAdapter()).getItemCount() - 1) {
                outRect.left = padding;
                outRect.right = padding;
                outRect.top = center;
                outRect.bottom = padding;
            } else {
                outRect.left = padding;
                outRect.right = padding;
                outRect.top = center;
                outRect.bottom = center;
            }
        }

    }
}

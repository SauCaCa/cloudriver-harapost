package cloudriver.vn.harapost.adapter;

public interface OnViewholderClickListener {
    void onClick(Object object, int position);
}

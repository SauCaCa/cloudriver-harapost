package cloudriver.vn.harapost.entries;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Created implements Parcelable {
    @SerializedName("date")
    String date;
    @SerializedName("timezone_type")
    String timezone_type;
    @SerializedName("timezone")
    String timezone;

    private Created(Parcel in) {
        date = in.readString();
        timezone_type = in.readString();
        timezone = in.readString();
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimezone_type() {
        return timezone_type;
    }

    public void setTimezone_type(String timezone_type) {
        this.timezone_type = timezone_type;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public static final Creator<Created> CREATOR = new Creator<Created>() {
        @Override
        public Created createFromParcel(Parcel in) {
            return new Created(in);
        }

        @Override
        public Created[] newArray(int size) {
            return new Created[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeString(timezone_type);
        dest.writeString(timezone);
    }
}

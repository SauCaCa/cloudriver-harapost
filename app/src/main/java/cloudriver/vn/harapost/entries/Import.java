package cloudriver.vn.harapost.entries;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Import implements Parcelable {
    @SerializedName("id")
    int id;
    @SerializedName("code")
    String code;
    @SerializedName("store_id")
    int store_id;
    @SerializedName("store_name")
    String store_name;
    @SerializedName("status")
    int status;
    @SerializedName("comfirm")
    String comfirm;
    @SerializedName("created_at")
    Created created_at;

    private Import(Parcel in) {
        id = in.readInt();
        code = in.readString();
        store_id = in.readInt();
        store_name = in.readString();
        status = in.readInt();
        comfirm = in.readString();
        created_at = in.readParcelable(Created.class.getClassLoader());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getComfirm() {
        return comfirm;
    }

    public void setComfirm(String comfirm) {
        this.comfirm = comfirm;
    }

    public Created getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Created created_at) {
        this.created_at = created_at;
    }

    public static final Creator<Import> CREATOR = new Creator<Import>() {
        @Override
        public Import createFromParcel(Parcel in) {
            return new Import(in);
        }

        @Override
        public Import[] newArray(int size) {
            return new Import[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(code);
        dest.writeInt(store_id);
        dest.writeString(store_name);
        dest.writeInt(status);
        dest.writeString(comfirm);
        dest.writeParcelable(created_at, flags);
    }
}

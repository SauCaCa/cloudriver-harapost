package cloudriver.vn.harapost.entries;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Order implements Parcelable {
    @SerializedName("id")
    int id;
    @SerializedName("code")
    String code;
    @SerializedName("name")
    String name;
    @SerializedName("sender_name")
    String sender_name;
    @SerializedName("sender_phone")
    String sender_phone;
    @SerializedName("sender_address")
    String sender_address;
    @SerializedName("sender_province")
    String sender_province;
    @SerializedName("sender_district")
    String sender_district;
    @SerializedName("receiver_name")
    String receiver_name;
    @SerializedName("receiver_phone")
    String receiver_phone;
    @SerializedName("receiver_address")
    String receiver_address;
    @SerializedName("receiver_district")
    String receiver_district;
    @SerializedName("receiver_province")
    String receiver_province;
    @SerializedName("status_id")
    int status_id;
    @SerializedName("status")
    String status;
    @SerializedName("amount")
    int amount;
    @SerializedName("payment_user")
    int payment_user;
    @SerializedName("method_ship")
    int method_ship;
    @SerializedName("cod")
    int cod;
    @SerializedName("fee_transport")
    int fee_transport;
    @SerializedName("fee_ship")
    int fee_ship;
    @SerializedName("fee_cod")
    int fee_cod;
    @SerializedName("fee_other")
    int fee_other;
    @SerializedName("fee_safe")
    int fee_safe;
    @SerializedName("total_fee")
    int total_fee;
    @SerializedName("created_at")
    Created created_at;


    private Order(Parcel in) {
        id = in.readInt();
        code = in.readString();
        name = in.readString();
        sender_name = in.readString();
        sender_phone = in.readString();
        sender_address = in.readString();
        sender_province = in.readString();
        sender_district = in.readString();
        receiver_name = in.readString();
        receiver_phone = in.readString();
        receiver_address = in.readString();
        receiver_district = in.readString();
        receiver_province = in.readString();
        status_id = in.readInt();
        status = in.readString();
        amount = in.readInt();
        payment_user = in.readInt();
        method_ship = in.readInt();
        cod = in.readInt();
        fee_transport = in.readInt();
        fee_ship = in.readInt();
        fee_cod = in.readInt();
        fee_other = in.readInt();
        fee_safe = in.readInt();
        total_fee = in.readInt();
        created_at = in.readParcelable(Created.class.getClassLoader());
    }

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getSender_name() {
        return sender_name;
    }

    public String getSender_phone() {
        return sender_phone;
    }

    public String getSender_address() {
        return sender_address;
    }

    public String getSender_province() {
        return sender_province;
    }

    public String getSender_district() {
        return sender_district;
    }

    public String getReceiver_name() {
        return receiver_name;
    }

    public String getReceiver_phone() {
        return receiver_phone;
    }

    public String getReceiver_address() {
        return receiver_address;
    }

    public String getReceiver_district() {
        return receiver_district;
    }

    public String getReceiver_province() {
        return receiver_province;
    }

    public int getStatus_id() {
        return status_id;
    }

    public String getStatus() {
        return status;
    }

    public int getAmount() {
        return amount;
    }

    public int getPayment_user() {
        return payment_user;
    }

    public int getMethod_ship() {
        return method_ship;
    }

    public int getCod() {
        return cod;
    }

    public int getFee_transport() {
        return fee_transport;
    }

    public int getFee_ship() {
        return fee_ship;
    }

    public int getFee_cod() {
        return fee_cod;
    }

    public int getFee_other() {
        return fee_other;
    }

    public int getFee_safe() {
        return fee_safe;
    }

    public int getTotal_fee() {
        return total_fee;
    }

    public Created getCreated_at() {
        return created_at;
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(code);
        dest.writeString(name);
        dest.writeString(sender_name);
        dest.writeString(sender_phone);
        dest.writeString(sender_address);
        dest.writeString(sender_province);
        dest.writeString(sender_district);
        dest.writeString(receiver_name);
        dest.writeString(receiver_phone);
        dest.writeString(receiver_address);
        dest.writeString(receiver_district);
        dest.writeString(receiver_province);
        dest.writeInt(status_id);
        dest.writeString(status);
        dest.writeInt(amount);
        dest.writeInt(payment_user);
        dest.writeInt(method_ship);
        dest.writeInt(cod);
        dest.writeInt(fee_transport);
        dest.writeInt(fee_ship);
        dest.writeInt(fee_cod);
        dest.writeInt(fee_other);
        dest.writeInt(fee_safe);
        dest.writeInt(total_fee);
        dest.writeParcelable(created_at, flags);
    }
}

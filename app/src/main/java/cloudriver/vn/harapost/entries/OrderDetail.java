package cloudriver.vn.harapost.entries;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class OrderDetail implements Parcelable {
    @SerializedName("order")
    Order order;
    @SerializedName("shipment")
    Shipment shipment;

    private OrderDetail(Parcel in) {
        order = in.readParcelable(Order.class.getClassLoader());
        shipment = in.readParcelable(Shipment.class.getClassLoader());
    }

    public static final Creator<OrderDetail> CREATOR = new Creator<OrderDetail>() {
        @Override
        public OrderDetail createFromParcel(Parcel in) {
            return new OrderDetail(in);
        }

        @Override
        public OrderDetail[] newArray(int size) {
            return new OrderDetail[size];
        }
    };

    public Order getOrder() {
        return order;
    }


    public Shipment getShipment() {
        return shipment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(order, flags);
        dest.writeParcelable(shipment, flags);
    }
}

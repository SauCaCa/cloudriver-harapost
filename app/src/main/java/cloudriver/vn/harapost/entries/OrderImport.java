package cloudriver.vn.harapost.entries;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class OrderImport implements Parcelable {
    @SerializedName("order")
    Order order;
    @SerializedName("import")
    Import _import;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Import getImport() {
        return _import;
    }

    public void setImport(Import _import) {
        this._import = _import;
    }

    private OrderImport(Parcel in) {
        order = in.readParcelable(Order.class.getClassLoader());
        _import = in.readParcelable(Import.class.getClassLoader());
    }

    public static final Creator<OrderImport> CREATOR = new Creator<OrderImport>() {
        @Override
        public OrderImport createFromParcel(Parcel in) {
            return new OrderImport(in);
        }

        @Override
        public OrderImport[] newArray(int size) {
            return new OrderImport[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(order, flags);
        dest.writeParcelable(_import, flags);
    }
}

package cloudriver.vn.harapost.entries;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PickedData implements Parcelable {
    @SerializedName("ids")
    String[] id;

    public PickedData(String[] id) {
        this.id = id;
    }

    private PickedData(Parcel in) {
        id = in.createStringArray();
    }

    public static final Creator<PickedData> CREATOR = new Creator<PickedData>() {
        @Override
        public PickedData createFromParcel(Parcel in) {
            return new PickedData(in);
        }

        @Override
        public PickedData[] newArray(int size) {
            return new PickedData[size];
        }
    };

    public String[] getId() {
        return id;
    }

    public void setId(String[] id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(id);
    }
}

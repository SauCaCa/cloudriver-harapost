package cloudriver.vn.harapost.entries;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Shipment implements Parcelable {
    @SerializedName("id")
    int id;
    @SerializedName("transport_type")
    int transport_type;
    @SerializedName("transport_user_id")
    int transport_user_id;
    @SerializedName("sender_name")
    String sender_name;
    @SerializedName("receiver_name")
    String receiver_name;
    @SerializedName("receiver_address")
    String receiver_address;
    @SerializedName("receiver_phone")
    String receiver_phone;
    @SerializedName("status")
    int status;
    @SerializedName("confirm")
    int confirm;
    @SerializedName("train_id")
    int train_id;
    @SerializedName("ship_time")
    String ship_time;
    @SerializedName("note")
    String note;
    @SerializedName("reason_cancel")
    String reason_cancel;


    private Shipment(Parcel in) {
        id = in.readInt();
        transport_type = in.readInt();
        transport_user_id = in.readInt();
        sender_name = in.readString();
        receiver_name = in.readString();
        receiver_address = in.readString();
        receiver_phone = in.readString();
        status = in.readInt();
        confirm = in.readInt();
        train_id = in.readInt();
        ship_time = in.readString();
        note = in.readString();
        reason_cancel = in.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTransport_type() {
        return transport_type;
    }

    public void setTransport_type(int transport_type) {
        this.transport_type = transport_type;
    }

    public int getTransport_user_id() {
        return transport_user_id;
    }

    public void setTransport_user_id(int transport_user_id) {
        this.transport_user_id = transport_user_id;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
    }

    public String getReceiver_address() {
        return receiver_address;
    }

    public void setReceiver_address(String receiver_address) {
        this.receiver_address = receiver_address;
    }

    public String getReceiver_phone() {
        return receiver_phone;
    }

    public void setReceiver_phone(String receiver_phone) {
        this.receiver_phone = receiver_phone;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getConfirm() {
        return confirm;
    }

    public void setConfirm(int confirm) {
        this.confirm = confirm;
    }

    public int getTrain_id() {
        return train_id;
    }

    public void setTrain_id(int train_id) {
        this.train_id = train_id;
    }

    public String getShip_time() {
        return ship_time;
    }

    public void setShip_time(String ship_time) {
        this.ship_time = ship_time;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getReason_cancel() {
        return reason_cancel;
    }

    public void setReason_cancel(String reason_cancel) {
        this.reason_cancel = reason_cancel;
    }

    public static final Creator<Shipment> CREATOR = new Creator<Shipment>() {
        @Override
        public Shipment createFromParcel(Parcel in) {
            return new Shipment(in);
        }

        @Override
        public Shipment[] newArray(int size) {
            return new Shipment[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(transport_type);
        dest.writeInt(transport_user_id);
        dest.writeString(sender_name);
        dest.writeString(receiver_name);
        dest.writeString(receiver_address);
        dest.writeString(receiver_phone);
        dest.writeInt(status);
        dest.writeInt(confirm);
        dest.writeInt(train_id);
        dest.writeString(ship_time);
        dest.writeString(note);
        dest.writeString(reason_cancel);
    }
}

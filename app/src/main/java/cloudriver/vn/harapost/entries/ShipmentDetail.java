package cloudriver.vn.harapost.entries;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShipmentDetail implements Parcelable {
    @SerializedName("shipment")
    Shipment shipment;
    @SerializedName("products")
    List<Product> products;

    public ShipmentDetail() {
    }

    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    private ShipmentDetail(Parcel in) {
        shipment = in.readParcelable(Shipment.class.getClassLoader());
        products = in.createTypedArrayList(Product.CREATOR);
    }

    public static final Creator<ShipmentDetail> CREATOR = new Creator<ShipmentDetail>() {
        @Override
        public ShipmentDetail createFromParcel(Parcel in) {
            return new ShipmentDetail(in);
        }

        @Override
        public ShipmentDetail[] newArray(int size) {
            return new ShipmentDetail[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(shipment, flags);
        dest.writeTypedList(products);
    }
}

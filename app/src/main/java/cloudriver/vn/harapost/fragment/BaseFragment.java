package cloudriver.vn.harapost.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.arellomobile.mvp.MvpAppCompatFragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BaseFragment extends MvpAppCompatFragment {
    private Unbinder mUnbinder;
    private CompositeDisposable disposableSet;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUnbinder = ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        mUnbinder.unbind();
        onUnsubscribe();
        mUnbinder = null;
        super.onDestroyView();
    }


    public void onUnsubscribe() {
        if (disposableSet != null) {
            disposableSet.clear();
            disposableSet = null;
        }
    }

    public void addDisposable(Disposable d) {
        if (disposableSet == null)
            disposableSet = new CompositeDisposable();
        disposableSet.add(d);
    }
}

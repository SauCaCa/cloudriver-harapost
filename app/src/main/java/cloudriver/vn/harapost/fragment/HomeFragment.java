package cloudriver.vn.harapost.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import cloudriver.vn.harapost.R;
import cloudriver.vn.harapost.view.OnOrderListener;

public class HomeFragment extends BaseFragment implements OnOrderListener {
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    HomeViewpagerAdapter adapter;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new HomeViewpagerAdapter(getChildFragmentManager(),
                FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onConfirm() {
        if (adapter != null && adapter.getCount() > 2 && adapter.getItem(1) instanceof ShipmentListFragment)
            ((ShipmentListFragment) adapter.getItem(1)).onRefresh();
    }

    @Override
    public void onStartShip() {
        if (adapter != null && adapter.getCount() > 2 && adapter.getItem(1) instanceof ShipmentListFragment)
            ((ShipmentListFragment) adapter.getItem(2)).onRefresh();
    }

    private class HomeViewpagerAdapter extends FragmentStatePagerAdapter {
        HomeViewpagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
//            Màn hình đơn mới: confirm = 0
//            Màn hình đang chờ nhận: confirm = 1, status=0
//            Màn hình đơn hàng đang giao: confirm = 1, status=2
            switch (position) {
                case 1:
                    return ShipmentListFragment.newInstance(1, 0);
                case 2:
                    return ShipmentListFragment.newInstance(1, 2);
                default:
                    return ShipmentListFragment.newInstance(0, 0);
            }
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 1:
                    return "Đơn hàng chờ nhận";
                case 2:
                    return "Đơn hàng đang vận chuyển";
                default:
                    return "Đơn hàng mới";
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}

package cloudriver.vn.harapost.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindDimen;
import butterknife.BindView;
import cloudriver.vn.harapost.R;
import cloudriver.vn.harapost.activity.DetailOrderActivity;
import cloudriver.vn.harapost.adapter.ItemOrderNewBinder;
import cloudriver.vn.harapost.adapter.ItemOrderPickBinder;
import cloudriver.vn.harapost.adapter.ItemOrderShipingBinder;
import cloudriver.vn.harapost.adapter.ListOrderDecoration;
import cloudriver.vn.harapost.adapter.OnViewholderClickListener;
import cloudriver.vn.harapost.entries.OrderDetail;
import cloudriver.vn.harapost.entries.PickedData;
import cloudriver.vn.harapost.presenter.ListOrderPresenter;
import cloudriver.vn.harapost.view.ListOrderView;
import cloudriver.vn.harapost.view.OnOrderListener;
import cloudriver.vn.harapost.widget.ProgressDialog;
import cloudriver.vn.harapost.widget.RecyclerAdapterWrapper;
import me.drakeet.multitype.MultiTypeAdapter;
import me.samlss.oops.Oops;

public class ShipmentListFragment extends BaseFragment implements ListOrderView, SwipeRefreshLayout.OnRefreshListener {

    @InjectPresenter
    ListOrderPresenter presenter;

    @BindView(R.id.root)
    FrameLayout root;
    @BindView(R.id.swipe_refresh_widget)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private Activity activity;
    private ProgressDialog progressDialog;
    private List<Object> list;
    private RecyclerAdapterWrapper adapterWrapper;
    private int page = 0;
    public static int LIMIT = 10;
    private int status, confirm;

    @BindDimen(R.dimen.space_10dp)
    int padding;

    private OnOrderListener orderChangeListener;
    private OnViewholderClickListener clicklistener,
            confirmListener, cancelListener,
            pickedListenter,
            successListener, failedListener;

    public static ShipmentListFragment newInstance(int confirm, int status) {
        ShipmentListFragment f = new ShipmentListFragment();
        Bundle b = new Bundle();
        b.putInt("status", status);
        b.putInt("confirm", confirm);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        orderChangeListener = ((HomeFragment) getParentFragment());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recycler_swiprefresh_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle b = getArguments();
        if (b != null) {
            status = b.getInt("status");
            confirm = b.getInt("confirm");
        }
        Oops.with(root).setGravity(Gravity.CENTER);
        initListener();
        initView();
    }

    private void initView() {
        swipeRefreshLayout.setOnRefreshListener(this);
        list = new ArrayList<>();
        MultiTypeAdapter adapter = new MultiTypeAdapter();
        adapter.register(OrderDetail.class)
                .to(new ItemOrderNewBinder(clicklistener, confirmListener, cancelListener),
                        new ItemOrderPickBinder(clicklistener, pickedListenter),
                        new ItemOrderShipingBinder(clicklistener, successListener, failedListener))
                .withLinker((position, shipmentDetail) -> {
                    if (shipmentDetail.getShipment().getConfirm() == 1 &&
                            shipmentDetail.getShipment().getStatus() == 0)
                        return 1;
                    if (shipmentDetail.getShipment().getConfirm() == 1 &&
                            shipmentDetail.getShipment().getStatus() == 2)
                        return 2;
                    return 0;
                });
        adapter.setItems(list);
        adapterWrapper = new RecyclerAdapterWrapper(adapter)
                .setLoadMoreState(RecyclerAdapterWrapper.LOADING)
                .setOnLoadMoreListener(() -> {
                    if (list.size() > 0)
                        page++;
                    loadData();
                });
        recyclerView.setPadding(0, 0, 0, padding);
        recyclerView.addItemDecoration(new ListOrderDecoration(padding));
        recyclerView.setAdapter(adapterWrapper);
    }

    private void initListener() {
        clicklistener = (detail, position) -> {
            Intent intent = new Intent(activity, DetailOrderActivity.class);
            intent.putExtra("order", ((OrderDetail) detail));
            startActivity(intent);
        };
        confirmListener = (detail, position) ->
                showConfirm("Bạn muốn nhận đơn hàng này?", "Nhận đơn", (dialog, which) -> presenter.confirm(((OrderDetail) detail), position));

        cancelListener = (detail, position) -> {
            View view = getLayoutInflater().inflate(R.layout.dialog_edt_confirm, null);
            EditText edt = view.findViewById(R.id.dialog_edt);
            edt.requestFocus();
            new AlertDialog.Builder(activity, R.style.dialog_keyboard)
                    .setView(view)
                    .setTitle("Xác nhận")
//                    .setMessage("Bạn muốn bỏ qua đơn hàng này?")
                    .setPositiveButton("Bỏ đơn", (dialog, which) -> {
                        String reason = edt.getText().toString();
                        if (TextUtils.isEmpty(reason)) {
                            Toast.makeText(activity, "Vui lòng nhập lý do", Toast.LENGTH_SHORT).show();
                        } else {
                            presenter.cancel(((OrderDetail) detail), edt.getText().toString(), position);
                            dialog.dismiss();
                        }
                    }).setNegativeButton("Hủy", (dialog, which) -> dialog.dismiss()).show();
        };

        pickedListenter = (detail, position) ->
                showConfirm("Bạn đã nhận được hàng?", "Đã nhận hàng", (dialog, which) -> {
                    PickedData data = new PickedData(new String[]{String.valueOf(((OrderDetail) detail).getShipment().getId())});
                    presenter.picked(data, position);
                });

        successListener = (detail, position) ->
                showConfirm("Đơn hàng đã được giao đến khách?", "Giao thành công", (dialog, which) -> {
                    presenter.success(((OrderDetail) detail), position);
                });

        failedListener = (detail, position) -> {
            View view = getLayoutInflater().inflate(R.layout.dialog_edt_confirm, null);
            EditText edt = view.findViewById(R.id.dialog_edt);
            edt.setHint("Lý do giao hàng thất bại?");
            edt.requestFocus();
            new AlertDialog.Builder(activity, R.style.dialog_keyboard)
                    .setView(view)
                    .setTitle("Xác nhận")
//                    .setMessage("Bạn muốn bỏ qua đơn hàng này?")
                    .setPositiveButton("Gửi", (dialog, which) -> {
                        String reason = edt.getText().toString();
                        if (TextUtils.isEmpty(reason)) {
                            Toast.makeText(activity, "Vui lòng nhập lý do", Toast.LENGTH_SHORT).show();
                        } else {
                            presenter.success(((OrderDetail) detail), edt.getText().toString(), position);
                            dialog.dismiss();
                        }
                    }).setNegativeButton("Thoát", (dialog, which) -> dialog.dismiss()).show();
        };
    }


    private void showConfirm(String message, String btn_confirm_text, DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(activity)
                .setTitle("Xác nhận")
                .setMessage(message)
                .setPositiveButton(btn_confirm_text, (dialog, which) -> {
                    if (listener != null)
                        listener.onClick(dialog, which);
                    dialog.dismiss();
                }).setNegativeButton("Hủy", (dialog, which) -> dialog.dismiss()).show();
    }

    @Override
    public void showDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(activity);
            progressDialog.setCancelable(false);
        }
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void hideDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    private void loadData() {
        if (presenter != null)
            presenter.getShipments(page, status, confirm);
    }

    @Override
    public void onSuccess(List<OrderDetail> datas) {
        Log.e("success", String.format("page: %s size: %s", page, datas.size()));
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);

        if (page == 0 && datas.size() == 0) {
            Oops.with(root)
                    .setIntercept(false)
                    .show(R.layout.layout_empty)
                    .setLayoutClickListener(v -> {
                        adapterWrapper.loading();
                        Oops.with(root).dismiss();
                        loadData();
                    });
        } else {
            Oops.with(root).dismiss();
            if (page == 0) {
                list.clear();
                list.addAll(datas);
                adapterWrapper.notifyDataSetChanged();
            } else {
                int count = list.size();
                list.addAll(datas);
                adapterWrapper.notifyItemRangeInserted(count, datas.size());
            }
        }
        adapterWrapper.loadingComplete();
        if (datas.size() >= LIMIT) {
            adapterWrapper.showLoadmore();
        } else adapterWrapper.hideLoadmore();
    }

    @Override
    public void onError(String message) {
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        if (page > 0) page--;
        adapterWrapper.loadMoreFailed(v -> {
            adapterWrapper.loading();
            page++;
            loadData();
        });
    }

    @Override
    public void confirmSuccess(OrderDetail detail, int position) {
        orderChangeListener.onConfirm();
        sucseed(position);
    }

    @Override
    public void confirmFailed(String error) {
        failed(error);
    }

    @Override
    public void cancelSuccess(OrderDetail detail, int position) {
        sucseed(position);
    }

    @Override
    public void cancelFailed(String error) {
        failed(error);
    }

    @Override
    public void pickSuccess(OrderDetail detail, int position) {
        orderChangeListener.onStartShip();
        sucseed(position);
    }

    @Override
    public void pickFailed(String error) {
        failed(error);
    }

    @Override
    public void doneSuccess(OrderDetail detail, int position) {
        sucseed(position);
    }

    @Override
    public void doneFailed(String error) {
        failed(error);
    }

    private void sucseed(int position) {
        Toast.makeText(activity, "Thành công", Toast.LENGTH_SHORT).show();
        list.remove(position);
        adapterWrapper.notifyItemRemoved(position);
        if (list.size() == 0) {
            Oops.with(root)
                    .setIntercept(false)
                    .show(R.layout.layout_empty)
                    .setLayoutClickListener(v -> {
                        adapterWrapper.loading();
                        Oops.with(root).dismiss();
                        loadData();
                    });
        }
    }

    private void failed(String error) {
        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        page = 0;
        loadData();
    }
}

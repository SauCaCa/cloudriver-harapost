package cloudriver.vn.harapost.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindDimen;
import butterknife.BindView;
import cloudriver.vn.harapost.R;
import cloudriver.vn.harapost.activity.DetailOrderActivity;
import cloudriver.vn.harapost.adapter.ItemStoreBinder;
import cloudriver.vn.harapost.adapter.ListOrderDecoration;
import cloudriver.vn.harapost.adapter.OnViewholderClickListener;
import cloudriver.vn.harapost.entries.OrderDetail;
import cloudriver.vn.harapost.entries.OrderImport;
import cloudriver.vn.harapost.presenter.StorekeeperPresenter;
import cloudriver.vn.harapost.view.StorekeeperView;
import cloudriver.vn.harapost.widget.ProgressDialog;
import cloudriver.vn.harapost.widget.RecyclerAdapterWrapper;
import me.drakeet.multitype.MultiTypeAdapter;
import me.samlss.oops.Oops;

public class StorekeeperFragment extends BaseFragment implements StorekeeperView, SwipeRefreshLayout.OnRefreshListener {

    @InjectPresenter
    StorekeeperPresenter presenter;

    @BindView(R.id.root)
    FrameLayout root;
    @BindView(R.id.swipe_refresh_widget)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private Activity activity;
    private ProgressDialog progressDialog;
    private List<Object> list;
    private RecyclerAdapterWrapper adapterWrapper;
    private int page = 0;
    public static int LIMIT = 10;

    @BindDimen(R.dimen.space_10dp)
    int padding;

    private OnViewholderClickListener clicklistener, confirmListener;

    public static StorekeeperFragment newInstance() {
        return new StorekeeperFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recycler_swiprefresh_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Oops.with(root).setGravity(Gravity.CENTER);
        initListener();
        initView();
    }

    private void initView() {
        swipeRefreshLayout.setOnRefreshListener(this);
        list = new ArrayList<>();
        MultiTypeAdapter adapter = new MultiTypeAdapter();
        adapter.register(OrderImport.class, new ItemStoreBinder(clicklistener, confirmListener));
        adapter.setItems(list);
        adapterWrapper = new RecyclerAdapterWrapper(adapter)
                .setLoadMoreState(RecyclerAdapterWrapper.LOADING)
                .setOnLoadMoreListener(() -> {
                    if (list.size() > 0)
                        page++;
                    loadData();
                });
        recyclerView.setPadding(0, 0, 0, padding);
        recyclerView.addItemDecoration(new ListOrderDecoration(padding));
        recyclerView.setAdapter(adapterWrapper);
    }

    private void initListener() {
        clicklistener = (detail, position) -> {
            Intent intent = new Intent(activity, DetailOrderActivity.class);
            intent.putExtra("import", ((OrderImport) detail));
            startActivity(intent);
        };
        confirmListener = (detail, position) ->
                showConfirm("Đơn hàng đã được nhập kho?", "Nhập kho", (dialog, which) -> presenter.confirm(((OrderImport) detail), position));
    }


    private void showConfirm(String message, String btn_confirm_text, DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(activity)
                .setTitle("Xác nhận")
                .setMessage(message)
                .setPositiveButton(btn_confirm_text, (dialog, which) -> {
                    if (listener != null)
                        listener.onClick(dialog, which);
                    dialog.dismiss();
                }).setNegativeButton("Hủy", (dialog, which) -> dialog.dismiss()).show();
    }

    @Override
    public void showDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(activity);
            progressDialog.setCancelable(false);
        }
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void hideDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void confirmSuccess(OrderImport detail, int position) {
        list.remove(position);
        adapterWrapper.notifyItemRemoved(position);
        if (list.size()==0){
            Oops.with(root)
                    .setIntercept(false)
                    .show(R.layout.layout_empty)
                    .setLayoutClickListener(v -> {
                        adapterWrapper.loading();
                        Oops.with(root).dismiss();
                        loadData();
                    });
        }
    }

    @Override
    public void confirmFailed(String error) {

    }

    private void loadData() {
        presenter.getOrderImports(page);
    }

    @Override
    public void onSuccess(List<OrderImport> datas) {
        Log.e("success", String.format("page: %s size: %s", page, datas.size()));
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);

        if (page == 0 && datas.size() == 0) {
            Oops.with(root)
                    .setIntercept(false)
                    .show(R.layout.layout_empty)
                    .setLayoutClickListener(v -> {
                        adapterWrapper.loading();
                        Oops.with(root).dismiss();
                        loadData();
                    });
        } else {
            Oops.with(root).dismiss();
            if (page == 0) {
                list.clear();
                list.addAll(datas);
                adapterWrapper.notifyDataSetChanged();
            } else {
                int count = list.size();
                list.addAll(datas);
                adapterWrapper.notifyItemRangeInserted(count, datas.size());
            }
        }
        adapterWrapper.loadingComplete();
        if (datas.size() >= LIMIT) {
            adapterWrapper.showLoadmore();
        } else adapterWrapper.hideLoadmore();
    }

    @Override
    public void onError(String message) {
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        if (page > 0) page--;
        adapterWrapper.loadMoreFailed(v -> {
            adapterWrapper.loading();
            page++;
            loadData();
        });
    }

    @Override
    public void onRefresh() {
        page = 0;
        loadData();
    }
}

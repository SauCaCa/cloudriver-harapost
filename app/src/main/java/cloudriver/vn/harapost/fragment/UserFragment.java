package cloudriver.vn.harapost.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.mpt.android.stv.Slice;
import com.mpt.android.stv.SpannableTextView;

import butterknife.BindColor;
import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.OnClick;
import cloudriver.vn.harapost.HApplication;
import cloudriver.vn.harapost.R;
import cloudriver.vn.harapost.activity.ChangePassActivity;
import cloudriver.vn.harapost.activity.LoginActivity;
import cloudriver.vn.harapost.activity.MainActivity;
import cloudriver.vn.harapost.entries.User;

public class UserFragment extends BaseFragment {
    @BindView(R.id.user_avatar)
    ImageView user_avatar;
    @BindView(R.id.user_info_text)
    SpannableTextView user_info_text;

    @BindColor(R.color.user_info_header)
    int color_info;
    @BindColor(R.color.grey_700)
    int color_header;

    @BindDimen(R.dimen.text_header)
    int textSize;

    MainActivity mainActivity;

    public static UserFragment newInstance() {
        return new UserFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_userinfo, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        User user = HApplication.getInstance().getAppStorage().getUser();
        if (!TextUtils.isEmpty(user.getAvatar())) {
            Glide.with(mainActivity).load(user.getAvatar()).into(user_avatar);
        } else user_avatar.setBackgroundResource(R.color.grey_300);

        user_info_text.reset();
        user_info_text.addSlice(new Slice.Builder("Họ tên: \n")
                .textColor(color_header)
                .build());
        user_info_text.addSlice(new Slice.Builder(user.getName() + "\n")
                .textColor(color_info)
                .textSize(textSize)
                .style(Typeface.BOLD)
                .build());
        user_info_text.addSlice(new Slice.Builder("\nVai trò: \n")
                .textColor(color_header)
                .build());
        user_info_text.addSlice(new Slice.Builder(HApplication.getInstance()
                .getAppDefine().getUserType(user.getType()) + "\n")
                .textColor(color_info)
                .style(Typeface.BOLD)
                .textSize(textSize)
                .build());
        user_info_text.addSlice(new Slice.Builder("\nEmail: \n")
                .textColor(color_header)
                .build());
        user_info_text.addSlice(new Slice.Builder(user.getEmail() + "\n")
                .textColor(color_info)
                .style(Typeface.BOLD)
                .textSize(textSize)
                .build());
        user_info_text.addSlice(new Slice.Builder("\nSố điện thoại: \n")
                .textColor(color_header)
                .build());
        user_info_text.addSlice(new Slice.Builder(user.getPhone() + "\n")
                .textColor(color_info)
                .style(Typeface.BOLD)
                .textSize(textSize)
                .build());
        user_info_text.addSlice(new Slice.Builder("\nĐịa chỉ: \n")
                .textColor(color_header)
                .build());
        String address = TextUtils.isEmpty(user.getAddress()) ? " \n" : user.getAddress() + "\n";
        user_info_text.addSlice(new Slice.Builder(address)
                .textColor(color_info)
                .style(Typeface.BOLD)
                .textSize(textSize)
                .build());

        user_info_text.display();
    }

    @OnClick({R.id.user_btn_logout, R.id.user_btn_change_pass})
    void onClick(View v) {
        if (v.getId() == R.id.user_btn_logout) {
            HApplication.getInstance().getAppStorage().clear();
            startActivity(new Intent(mainActivity, LoginActivity.class));
            mainActivity.finish();
        } else {
            startActivity(new Intent(mainActivity, ChangePassActivity.class));
        }
    }
}

package cloudriver.vn.harapost.network;

import java.util.List;

import cloudriver.vn.harapost.entries.OrderDetail;
import cloudriver.vn.harapost.entries.OrderImport;
import cloudriver.vn.harapost.entries.PickedData;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {
    String URL = "http://uat.harapost.vn/";
    /*
    trạng thái shipment [
        0 => 'Mới tạo',
        2 => 'Đã lấy hàng',
        1 => 'Giao hàng thành công',
        3 => 'Không giao được hàng',
        -1 => 'Hủy',
    ]

    trạng thái xác nhận vận chuyển shipment [
        0 => 'Chưa xác nhận',
        1 => 'Đã xác nhận',
        -1 => 'Từ chối vận chuyển',
    ]
    */

    @FormUrlEncoded
    @POST("/api/login")
    Observable<ResponseBody> login(
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("/api/shipments/")
    Observable<List<OrderDetail>> shipmentsList(
            @Field("offset") int offset,
            @Field("limit") int limit,
            @Field("status") int status,
            @Field("confirm") int confirm
    );

    @FormUrlEncoded
    @POST("/api/shipments/{id}/confirm")
    Observable<ResponseBody> shipmentConfirm(
            @Path("id") int id,
            @Field("confirm") int confirm
    );

    @FormUrlEncoded
    @POST("/api/shipments/{id}/confirm")
    Observable<ResponseBody> shipmentCancel(
            @Path("id") int id,
            @Field("status") int status,
            @Field("reason_cancel") String reason
    );

    @POST("/api/shipments/start/shipment")
    Observable<ResponseBody> shipmentStart(
            @Body PickedData ids
    );


    @POST("/api/shipments/{id}/success")
    Observable<ResponseBody> shipmentSuccess(
            @Path("id") int id
    );

    @FormUrlEncoded
    @POST("/api/shipments/{id}/success")
    Observable<ResponseBody> shipmentFailed(
            @Path("id") int id,
            @Field("status") int status,
            @Field("reason_cancel") String reason_cancel
    );

    @FormUrlEncoded
    @POST("/api/orders/check-qrcode")
    Observable<OrderDetail> qrDetail(
            @Field("qrcode") String qrcode
    );


    //---------------------------
    //define
    //---------------------------
    @POST("/api/defined")
    Observable<ResponseBody> getDefined();

    @FormUrlEncoded
    @POST("/api/change/password")
    Observable<ResponseBody> changePass(
            @Field("old_password") String old_password,
            @Field("new_password") String new_password
    );

    //---------------------------
    //Storekeeper
    //---------------------------
    @FormUrlEncoded
    @POST("/api/imports")
    Observable<List<OrderImport>> storeList(
            @Field("offset") int offset,
            @Field("limit") int limit);

    @FormUrlEncoded
    @POST("/api/imports/check-qrcode")
    Observable<OrderImport> storeQrDetail(
            @Field("qrcode") String qrcode
    );

    @POST("/api/imports/confirm")
    Observable<ResponseBody> storeConfirm(
            @Body PickedData ids
    );

}

package cloudriver.vn.harapost.presenter;

import android.util.Log;

import androidx.annotation.NonNull;

import com.arellomobile.mvp.MvpPresenter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Objects;

import cloudriver.vn.harapost.view.ActionValue;
import cloudriver.vn.harapost.view.BaseView;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;
import retrofit2.HttpException;


public class BasePresenter<View extends BaseView> extends MvpPresenter<View> {
    private CompositeDisposable disposableSet;

    protected void unsubscribeOnDestroy(@NonNull Disposable d) {
        if (disposableSet == null) {
            disposableSet = new CompositeDisposable();
        }
        disposableSet.add(d);
    }

    @Override
    public void onDestroy() {
        if (disposableSet != null) {
            disposableSet.clear();
            disposableSet = null;
        }
        super.onDestroy();
    }

    protected void removeSubcribe(Disposable d) {
        if (disposableSet != null && disposableSet.size() > 0)
            disposableSet.remove(d);
    }

    void handleError(Throwable throwable) {
        Log.e("handleError", "handleError");
        throwable.printStackTrace();
        if (throwable instanceof HttpException)
            try {
                ResponseBody responseBody = Objects.requireNonNull(((HttpException) throwable).response()).errorBody();
                getViewState().onError(new JSONObject(Objects.requireNonNull(responseBody).string()).optString("message"));
            } catch (Exception e) {
                e.printStackTrace();
                getViewState().onError(throwable.getMessage());
            }
        else
            getViewState().onError(throwable.getMessage());
    }

    void handleError(Throwable throwable, ActionValue<String> actionValue) {
        throwable.printStackTrace();
        if (throwable instanceof HttpException) {
            try {
                ResponseBody responseBody = Objects.requireNonNull(((HttpException) throwable).response()).errorBody();
                actionValue.onAction(new JSONObject(Objects.requireNonNull(responseBody).string()).optString("message"));
            } catch (Exception e) {
                e.printStackTrace();
                actionValue.onAction(throwable.getMessage());
            }
        } else {
            actionValue.onAction(throwable.getMessage());
        }
    }
}

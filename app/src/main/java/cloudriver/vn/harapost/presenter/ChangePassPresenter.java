package cloudriver.vn.harapost.presenter;

import com.google.gson.Gson;

import org.json.JSONObject;

import cloudriver.vn.harapost.HApplication;
import cloudriver.vn.harapost.entries.User;
import cloudriver.vn.harapost.view.ChangePassView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import com.arellomobile.mvp.InjectViewState;

@InjectViewState
public class ChangePassPresenter extends BasePresenter<ChangePassView> {

    public void changePass(String oldPass, String newPass) {
        unsubscribeOnDestroy(HApplication.getInstance().getApiService().changePass(oldPass, newPass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseBody -> {
                    JSONObject jsonObject = new JSONObject(responseBody.string());
                    boolean success = jsonObject.optBoolean("result");
                    if (success)
                        getViewState().onSuccess(success);
                    else {
                        String error = "Vui lòng thử lại";
                        if (jsonObject.has("message")) {
                            error = jsonObject.optString("message");
                        }
                        getViewState().onError(error);
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                    handleError(throwable);
                }));
    }
}

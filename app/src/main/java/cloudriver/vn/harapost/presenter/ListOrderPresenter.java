package cloudriver.vn.harapost.presenter;

import com.arellomobile.mvp.InjectViewState;

import org.json.JSONObject;

import cloudriver.vn.harapost.HApplication;
import cloudriver.vn.harapost.entries.OrderDetail;
import cloudriver.vn.harapost.entries.PickedData;
import cloudriver.vn.harapost.fragment.ShipmentListFragment;
import cloudriver.vn.harapost.view.ListOrderView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class ListOrderPresenter extends BasePresenter<ListOrderView> {

    public void getShipments(int page, int status, int confirm) {
        unsubscribeOnDestroy(
                HApplication.getInstance().getApiService()
                        .shipmentsList(page, ShipmentListFragment.LIMIT, status, confirm)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(list -> getViewState().onSuccess(list), this::handleError)
        );
    }

    public void confirm(OrderDetail detail, int position) {
        getViewState().showDialog();
        unsubscribeOnDestroy(HApplication.getInstance().getApiService()
                .shipmentConfirm(detail.getShipment().getId(), 1).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseBody -> {
                            getViewState().hideDialog();
                            JSONObject result = new JSONObject(responseBody.string());
                            if (result.optBoolean("success"))
                                getViewState().confirmSuccess(detail, position);
                            else
                                getViewState().confirmFailed(result.optString("message"));
                        }, throwable -> {
                            getViewState().hideDialog();
                            handleError(throwable, value -> getViewState().confirmFailed(value));
                        }
                ));
    }

    public void cancel(OrderDetail detail, String reason, int position) {
        getViewState().showDialog();
        unsubscribeOnDestroy(HApplication.getInstance().getApiService()
                .shipmentCancel(detail.getShipment().getId(), -1, reason).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseBody -> {
                            getViewState().hideDialog();
                            JSONObject result = new JSONObject(responseBody.string());
                            if (result.optBoolean("success"))
                                getViewState().cancelSuccess(detail, position);
                            else
                                getViewState().cancelFailed(result.optString("message"));
                        }, throwable -> {
                            getViewState().hideDialog();
                            handleError(throwable, value -> getViewState().cancelFailed(value));
                        }
                ));
    }

    public void picked(PickedData data, int position) {
        getViewState().showDialog();
        unsubscribeOnDestroy(HApplication.getInstance().getApiService()
                .shipmentStart(data)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        responseBody -> {
                            getViewState().hideDialog();
                            JSONObject result = new JSONObject(responseBody.string());
                            if (result.optBoolean("success"))
                                getViewState().pickSuccess(null, position);
                            else
                                getViewState().pickFailed(result.optString("message"));
                        }, throwable -> {
                            getViewState().hideDialog();
                            handleError(throwable, value -> getViewState().pickFailed(value));
                        }
                ));
    }

    public void success(OrderDetail detail, int position) {
        getViewState().showDialog();
        unsubscribeOnDestroy(HApplication.getInstance().getApiService()
                .shipmentSuccess(detail.getShipment().getId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseBody -> {
                            getViewState().hideDialog();
                            JSONObject result = new JSONObject(responseBody.string());
                            if (result.optBoolean("success"))
                                getViewState().confirmSuccess(detail, position);
                            else
                                getViewState().confirmFailed(result.optString("message"));
                        }, throwable -> {
                            getViewState().hideDialog();
                            handleError(throwable, value -> getViewState().confirmFailed(value));
                        }
                ));
    }

    public void success(OrderDetail detail, String reason, int position) {
        getViewState().showDialog();
        unsubscribeOnDestroy(HApplication.getInstance().getApiService()
                .shipmentFailed(detail.getShipment().getId(), -1, reason).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseBody -> {
                            getViewState().hideDialog();
                            JSONObject result = new JSONObject(responseBody.string());
                            if (result.optBoolean("success"))
                                getViewState().confirmSuccess(detail, position);
                            else
                                getViewState().confirmFailed(result.optString("message"));
                        }, throwable -> {
                            getViewState().hideDialog();
                            handleError(throwable, value -> getViewState().confirmFailed(value));
                        }
                ));
    }
}

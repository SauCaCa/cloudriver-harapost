package cloudriver.vn.harapost.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;

import org.json.JSONObject;

import cloudriver.vn.harapost.HApplication;
import cloudriver.vn.harapost.entries.User;
import cloudriver.vn.harapost.view.LoginView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class LoginPresenter extends BasePresenter<LoginView> {

    public void login(String email, String password) {
        unsubscribeOnDestroy(HApplication.getInstance().getApiService().login(email, password)
                .map(responseBody -> {
                    JSONObject jsonObject = new JSONObject(responseBody.string()).optJSONObject("result");
                    String token = jsonObject.optString("token");
                    HApplication.getInstance().getAppStorage().saveToken(token);
                    String userStr = jsonObject.optString("user_info");
                    User user = new Gson().fromJson(userStr, User.class);
                    HApplication.getInstance().getAppStorage().saveUser(user);
                    HApplication.getInstance().refreshToken();
                    return true;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isSaved -> {
                    if (isSaved)
                        getViewState().onSuccess("");
                    else getViewState().onError("Vui lòng thử lại");
                }, throwable -> {
                    throwable.printStackTrace();
                    handleError(throwable);
                }));
    }
}

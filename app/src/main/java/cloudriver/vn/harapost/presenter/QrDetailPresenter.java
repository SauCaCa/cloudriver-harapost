package cloudriver.vn.harapost.presenter;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;

import org.json.JSONObject;

import cloudriver.vn.harapost.HApplication;
import cloudriver.vn.harapost.entries.PickedData;
import cloudriver.vn.harapost.view.QrDetailView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class QrDetailPresenter extends BasePresenter<QrDetailView> {

    public void getQrDetail(String code) {
        getViewState().showLoading();
        if (HApplication.getInstance().isShipper())
            unsubscribeOnDestroy(HApplication.getInstance().getApiService().qrDetail(code)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(order -> getViewState().onSuccess(order), this::handleError
                    ));
        else
            unsubscribeOnDestroy(HApplication.getInstance().getApiService().storeQrDetail(code)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(order -> getViewState().onSuccess(order), this::handleError
                    ));
    }

    public void confirmList(PickedData data) {
        getViewState().showLoading();
        if (HApplication.getInstance().isShipper())
            unsubscribeOnDestroy(HApplication.getInstance().getApiService()
                    .shipmentStart(data)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            responseBody -> {
                                getViewState().hideLoading();
                                JSONObject result = new JSONObject(responseBody.string());
                                if (result.optBoolean("success"))
                                    getViewState().confirmSuccess();
                                else
                                {
                                    Log.e("handleError", "shipmentStart responseBody");
                                    getViewState().confirmFailed(result.optString("message"));
                                }
                            }, throwable -> {
                                getViewState().hideLoading();
                                handleError(throwable, value -> getViewState().confirmFailed(value));
                            }
                    ));
        else
            unsubscribeOnDestroy(HApplication.getInstance().getApiService()
                    .storeConfirm(data)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            responseBody -> {
                                getViewState().hideLoading();
                                JSONObject result = new JSONObject(responseBody.string());
                                if (result.optBoolean("success"))
                                    getViewState().confirmSuccess();
                                else
                                {
                                    Log.e("handleError", "storeConfirm responseBody");
                                    getViewState().confirmFailed(result.optString("message"));
                                }
                            }, throwable -> {
                                getViewState().hideLoading();
                                handleError(throwable, value -> getViewState().confirmFailed(value));
                            }
                    ));
    }

}

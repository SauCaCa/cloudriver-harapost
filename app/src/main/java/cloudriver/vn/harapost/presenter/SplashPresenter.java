package cloudriver.vn.harapost.presenter;

import com.arellomobile.mvp.InjectViewState;

import org.json.JSONObject;

import cloudriver.vn.harapost.HApplication;
import cloudriver.vn.harapost.view.SplashView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class SplashPresenter extends BasePresenter<SplashView> {

    public void getDefine() {
        unsubscribeOnDestroy(
                HApplication.getInstance().getApiService().getDefined()
                        .map(responseBody -> {
                            String jsonObject = new JSONObject(responseBody.string()).getString("result");
                            HApplication.getInstance().getAppDefine().saveDefined(jsonObject);
                            return true;
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(result -> getViewState().onSuccess(result)
                                , this::handleError)
        );
    }
}

package cloudriver.vn.harapost.presenter;

import com.arellomobile.mvp.InjectViewState;

import org.json.JSONObject;

import java.util.Arrays;

import cloudriver.vn.harapost.HApplication;
import cloudriver.vn.harapost.entries.OrderDetail;
import cloudriver.vn.harapost.entries.OrderImport;
import cloudriver.vn.harapost.entries.PickedData;
import cloudriver.vn.harapost.fragment.ShipmentListFragment;
import cloudriver.vn.harapost.view.ListOrderView;
import cloudriver.vn.harapost.view.StorekeeperView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class StorekeeperPresenter extends BasePresenter<StorekeeperView> {

    public void getOrderImports(int page) {
        unsubscribeOnDestroy(
                HApplication.getInstance().getApiService()
                        .storeList(page, ShipmentListFragment.LIMIT)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(list -> getViewState().onSuccess(list), this::handleError)
        );
    }

    public void confirm(OrderImport detail, int position) {
        getViewState().showDialog();

        PickedData picker = new PickedData(new String[]{String.valueOf(detail.getImport().getId())});

        unsubscribeOnDestroy(HApplication.getInstance().getApiService()
                .storeConfirm(picker)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseBody -> {
                            getViewState().hideDialog();
                            JSONObject result = new JSONObject(responseBody.string());
                            if (result.optBoolean("success"))
                                getViewState().confirmSuccess(detail, position);
                            else
                                getViewState().confirmFailed(result.optString("message"));
                        }, throwable -> {
                            getViewState().hideDialog();
                            handleError(throwable, value -> getViewState().confirmFailed(value));
                        }
                ));
    }
}

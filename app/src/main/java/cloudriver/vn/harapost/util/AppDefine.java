package cloudriver.vn.harapost.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class AppDefine {
    private static final String TAG = "AppDefine";

    private static final String DEF_USER_TYPE = "user_type";
    private static final String DEF_PAYMENT_USER = "payment_user";
    private static final String DEF_METHOD_PAYMENT_FEE = "method_payment_fee";
    private static final String DEF_METHOD_PAYMENT_COD = "method_payment_cod";
    private static final String DEF_METHOD_SHIP = "method_ship";
    private static final String DEF_SHIP_TYPE = "ship_type";
    private static final String DEF_SHIP_STATUS = "shipment_status";
    private static final String DEF_SHIP_TRANSPORT_METHOD = "shipment_transport_method";
    private static final String DEF_ORDER_STATUS = "order_status";
    private static final String DEF_IMPORT_STATUS = "import_status";

    private String[] defined = new String[]{DEF_USER_TYPE,
            DEF_PAYMENT_USER,
            DEF_METHOD_PAYMENT_FEE,
            DEF_METHOD_PAYMENT_COD,
            DEF_METHOD_SHIP,
            DEF_SHIP_TYPE,
            DEF_SHIP_STATUS,
            DEF_SHIP_TRANSPORT_METHOD,
            DEF_ORDER_STATUS,
            DEF_IMPORT_STATUS,
    };

    private SharedPreferences preferences;

    public AppDefine(Context context) {
        preferences = context.getSharedPreferences("define", Context.MODE_PRIVATE);
    }

    public void saveDefined(String json) throws JSONException {
        JSONObject obj = new JSONObject(json);
        for (String s : defined) {
            preferences.edit().putString(s, obj.getString(s)).apply();
        }
    }

    public String getUserType(int type) {
        return fromDefineValue(DEF_USER_TYPE, type);
    }

    public String getPaymentUser(int type) {
        return fromDefineValue(DEF_PAYMENT_USER, type);
    }

    public String getMethodPaymentFee(int type) {
        return fromDefineValue(DEF_METHOD_PAYMENT_FEE, type);
    }

    public String getMethodPaymentCod(int type) {
        return fromDefineValue(DEF_METHOD_PAYMENT_COD, type);
    }

    public String getMethodShip(int type) {
        return fromDefineValue(DEF_METHOD_SHIP, type);
    }

    public String getShipType(int type) {
        return fromDefineValue(DEF_SHIP_TYPE, type);
    }

    public String getShipStatus(int type) {
        return fromDefineValue(DEF_SHIP_STATUS, type);
    }

    public String getShipTransportMethod(int type) {
        return fromDefineValue(DEF_SHIP_TRANSPORT_METHOD, type);
    }

    public String getOrderStatus(String type) {
        return fromDefineValue(DEF_ORDER_STATUS, Integer.valueOf(type));
    }

    public String getOrderStatus(int type) {
        return fromDefineValue(DEF_ORDER_STATUS, type);
    }

    public String getImportStatus(int type) {
        return fromDefineValue(DEF_IMPORT_STATUS, type);
    }

    private String fromDefineValue(String def, int key) {
        String value = "";
        String json = preferences.getString(def, "");
        if (!TextUtils.isEmpty(json)) {
            try {
                JSONObject jsonObject = new JSONObject(json);
                value = jsonObject.getString(String.valueOf(key));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    public void clear() {
        preferences.edit().clear().apply();
    }
}

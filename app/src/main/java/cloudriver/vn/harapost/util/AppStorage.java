package cloudriver.vn.harapost.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.gson.Gson;

import cloudriver.vn.harapost.entries.User;

public class AppStorage {
    private SharedPreferences preferences;
    private Gson gson;

    public AppStorage(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        gson = new Gson();
    }

    public void saveToken(String token) {
        preferences.edit().putString(Const.TOKEN, token).apply();
    }

    public String getToken() {
        return preferences.getString(Const.TOKEN, "");
    }

    public boolean isLoggedin() {
        return !TextUtils.isEmpty(preferences.getString(Const.TOKEN, ""));
    }

    public void saveUser(User user) {
        preferences.edit().putString(Const.USER, gson.toJson(user)).apply();
    }

    public User getUser() {
        String userStr = preferences.getString(Const.USER, "");
        if (!TextUtils.isEmpty(userStr)) {
            return gson.fromJson(userStr, User.class);
        }
        return null;
    }

    public void clear() {
        preferences.edit().clear().apply();
    }
}

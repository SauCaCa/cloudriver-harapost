package cloudriver.vn.harapost.util;

public class Const {
    public static final String USER = "user";
    public static final String TOKEN = "token";
    public static final String QRCODE = "qrcode";

    public static final long TIME_OUT = 15;

}

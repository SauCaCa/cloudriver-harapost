package cloudriver.vn.harapost.util;

import android.text.TextUtils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

public class Utils {


    public static String moneyFormat(int price) {
//        return moneyFormat(price, "đ");
        return moneyFormat(price, "");
    }

    public static String moneyFormat(int priceNumber, String currency) {
        return moneyFormat(String.valueOf(priceNumber), currency);
    }


    public static String moneyFormat(String price, String currency) {
        NumberFormat format =
                new DecimalFormat("#,##0.00");// #,##0.00 ¤ (¤:// Currency symbol)
        format.setCurrency(Currency.getInstance(Locale.getDefault()));//Or default locale

        price = (!TextUtils.isEmpty(price)) ? price : "0";
        price = price.trim();
        price = format.format(Double.parseDouble(price));
        price = price.replaceAll(",", "\\.");

        if (price.endsWith(".00")) {
            int centsIndex = price.lastIndexOf(".00");
            if (centsIndex != -1) {
                price = price.substring(0, centsIndex);
            }
        }
        price = TextUtils.isEmpty(currency) ? String.format("%s", price) : String.format("%s %s", price, currency);
        return price;
    }
}

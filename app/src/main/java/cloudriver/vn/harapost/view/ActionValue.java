package cloudriver.vn.harapost.view;

public interface ActionValue<V> {
    void onAction(V value);
}

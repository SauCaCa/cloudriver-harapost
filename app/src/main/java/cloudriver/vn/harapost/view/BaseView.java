package cloudriver.vn.harapost.view;

import com.arellomobile.mvp.MvpView;

public interface BaseView<D> extends MvpView {
    void onSuccess(D data);

    void onError(String error);
}

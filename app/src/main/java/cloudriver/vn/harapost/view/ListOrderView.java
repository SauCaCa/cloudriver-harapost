package cloudriver.vn.harapost.view;

import java.util.List;

import cloudriver.vn.harapost.entries.OrderDetail;

public interface ListOrderView extends BaseView<List<OrderDetail>> {

    void showDialog();

    void hideDialog();

    void confirmSuccess(OrderDetail detail, int position);

    void confirmFailed(String error);

    void cancelSuccess(OrderDetail detail, int position);

    void cancelFailed(String error);

    void pickSuccess(OrderDetail detail, int position);

    void pickFailed(String error);

    void doneSuccess(OrderDetail detail, int position);

    void doneFailed(String error);
}

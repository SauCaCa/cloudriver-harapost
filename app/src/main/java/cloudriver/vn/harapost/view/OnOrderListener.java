package cloudriver.vn.harapost.view;

public interface OnOrderListener {
    void onConfirm();

    void onStartShip();
}

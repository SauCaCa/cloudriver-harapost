package cloudriver.vn.harapost.view;

import cloudriver.vn.harapost.entries.OrderDetail;
import cloudriver.vn.harapost.entries.OrderImport;

public interface QrDetailView extends BaseView<OrderDetail> {
    void showLoading();

    void hideLoading();

    void onSuccess(OrderImport data);

    void confirmSuccess();

    void confirmFailed(String error);
}

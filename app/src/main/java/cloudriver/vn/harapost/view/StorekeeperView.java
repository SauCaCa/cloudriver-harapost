package cloudriver.vn.harapost.view;

import java.util.List;

import cloudriver.vn.harapost.entries.OrderDetail;
import cloudriver.vn.harapost.entries.OrderImport;

public interface StorekeeperView extends BaseView<List<OrderImport>> {

    void showDialog();

    void hideDialog();

    void confirmSuccess(OrderImport detail, int position);

    void confirmFailed(String error);
}

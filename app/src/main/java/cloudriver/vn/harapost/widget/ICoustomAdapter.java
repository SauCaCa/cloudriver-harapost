package cloudriver.vn.harapost.widget;

import androidx.recyclerview.widget.RecyclerView;

public interface ICoustomAdapter {
    void onViewAttachedToWindow(RecyclerView.ViewHolder holder, int postion);
}

package cloudriver.vn.harapost.widget;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.IntDef;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import cloudriver.vn.harapost.R;


public class RecyclerAdapterWrapper extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ICoustomAdapter {
    public static final int ITEM_TYPE_LOAD_MORE = 102;

    public static final int NONE = -1;
    public static final int LOADING = 0;
    public static final int FAILED = 1;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({NONE, LOADING, FAILED})
    public @interface FooterState {
    }

    private RecyclerView.Adapter mInnerAdapter;

    @FooterState
    private int mFooterState = NONE;
    private boolean isLoading = false;

    public RecyclerAdapterWrapper(RecyclerView.Adapter adapter) {
        mInnerAdapter = adapter;
    }

    private boolean hasLoadMore() {
        return mFooterState != NONE;
    }

    private boolean isShowLoadMore(int position) {
        return hasLoadMore() && (position >= mInnerAdapter.getItemCount());
    }

    @Override
    public int getItemViewType(int position) {
        if (isShowLoadMore(position)) {
            return ITEM_TYPE_LOAD_MORE;
        }
        return mInnerAdapter.getItemViewType(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_LOAD_MORE:
                return ViewHolder.createViewHolder(parent.getContext(), parent, R.layout.view_loadmore_layout);
            default:
                return mInnerAdapter.onCreateViewHolder(parent, viewType);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (isShowLoadMore(position)) {
            if (mFooterState == LOADING) {
                ((ViewHolder) holder).setVisible(R.id.loadmore_failed_view, false);
                ((ViewHolder) holder).setVisible(R.id.loadmore_progress_view, true);
                if (mOnLoadMoreListener != null && !isLoading) {
                    isLoading = true;
                    mOnLoadMoreListener.loadMore();
                }
            } else {
                ((ViewHolder) holder).setVisible(R.id.loadmore_progress_view, false);
                ((ViewHolder) holder).setVisible(R.id.loadmore_failed_view, true);
                if (retryListener != null) {
                    ((ViewHolder) holder).setVisible(R.id.loadmore_failed_button, true);
                    ((ViewHolder) holder).getView(R.id.loadmore_failed_button).setOnClickListener(retryListener);
                } else {
                    ((ViewHolder) holder).setVisible(R.id.loadmore_failed_button, false);
                }
            }
            return;
        }
        mInnerAdapter.onBindViewHolder(holder, position);
    }

    @Override
    public long getItemId(int position) {
        if (isShowLoadMore(position))
            return ("loadmorewrapper").hashCode();
        else
            return super.getItemId(position);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        WrapperUtils.onAttachedToRecyclerView(mInnerAdapter, recyclerView, new WrapperUtils.SpanSizeCallback() {
            @Override
            public int getSpanSize(GridLayoutManager layoutManager, GridLayoutManager.SpanSizeLookup oldLookup, int position) {
                if (isShowLoadMore(position)) {
                    return layoutManager.getSpanCount();
                }
                if (oldLookup != null) {
                    return oldLookup.getSpanSize(position);
                }
                return 1;
            }
        });
    }


    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        if (isShowLoadMore(holder.getLayoutPosition())) {
            setFullSpan(holder);
        } else {
            onViewAttachedToWindow(holder, holder.getLayoutPosition());
        }
    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder, int postion) {
        if (isShowLoadMore(holder.getLayoutPosition())) {
            setFullSpan(holder);
            return;
        }
        if (mInnerAdapter instanceof ICoustomAdapter) {
            ((ICoustomAdapter) mInnerAdapter).onViewAttachedToWindow(holder, postion);
            return;
        }
//        mInnerAdapter.onViewAttachedToWindow(holder);

    }

    private void setFullSpan(RecyclerView.ViewHolder holder) {
        WrapperUtils.setFullSpan(holder);
    }

    @Override
    public int getItemCount() {
        return mInnerAdapter.getItemCount() + (hasLoadMore() ? 1 : 0);
    }

    //----------------------------------------------------------------------------------------------------------
    // Loadmore method
    //----------------------------------------------------------------------------------------------------------
    //
    public interface LoadMoreListener {
        void loadMore();
    }

    private LoadMoreListener mOnLoadMoreListener;
    private View.OnClickListener retryListener;

    public RecyclerAdapterWrapper setOnLoadMoreListener(LoadMoreListener loadMoreListener) {
        mOnLoadMoreListener = loadMoreListener;
        return this;
    }

    public RecyclerAdapterWrapper setLoadMoreState(@FooterState int state) {
        this.mFooterState = state;
        return this;
    }

    public void hideLoadmore() {
        notifyItemRemoved(getItemCount() - 1);
        isLoading = false;
        mFooterState = NONE;
    }

    public void showLoadmore() {
        mFooterState = LOADING;
        notifyItemChanged(getItemCount() - 1);
    }

    public void loadMoreFailed(View.OnClickListener listener) {
        isLoading = false;
        this.mFooterState = FAILED;
        this.retryListener = listener;
        notifyItemChanged(getItemCount() - 1);
    }

    public void loading() {
        isLoading = true;
        this.mFooterState = LOADING;
        notifyItemChanged(getItemCount() - 1);
    }

    public void loadingComplete() {
        isLoading = false;
    }

    public boolean isLoading() {
        return isLoading;
    }

}
